#!/usr/bin/python3
# -*- coding: utf-8; mode: python; tab-width: 4; -*-

"""
Ubuntu Sponsorship Miner

Search for Ubuntu package sponsors and sponsorees.
"""

import sys
import os
sys.path.insert(0, os.path.abspath('../../pylibs/'))
from cgi_helpers import *
import cgi
import html
import sys, re, codecs
import textwrap

if sys.version_info < (3,0):
    from urllib import urlencode
else:
    from urllib.parse import urlencode

import psycopg2
import psycopg2.extensions


DATABASE = 'service=udd'
URL = 'ubuntu-sponsorships.cgi'
STATIC_URL = '../static'
UDD_URL = 'https://udd.debian.org/'
SOURCE_URL = 'https://salsa.debian.org/qa/udd/tree/master/web/cgi-bin/ubuntu-sponsorships.cgi'
LIMIT = 1000


class AttrDict(dict):
    """Dictionary with attribute access"""

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            self[key] = value

    def __getattr__(self, name):
        try:
            return self[name]
        except KeyError as e:
            raise AttributeError(e)


form_template = textwrap.dedent("""\
    <!DOCTYPE html>
    <html>
        <head>
            <title>Ubuntu Sponsorship Miner</title>
            <style type="text/css">
                #footer {
                    clear: left;
                    padding-top: 2em;
                }
                label.left {
                    clear: left;
                    display: block;
                    width: 8em;
                }
                input.button {
                    clear: left;
                    margin-top: 1em;
                }
                form div {
                    clear: left;
                }
                form * {
                    float: left;
                }
            </style>
        </head>
        <body>
            <h1>Ubuntu Sponsorship Miner</h1>
            <div id="content">
                <h2>Search for sponsorships:</h2>
                <form action="%(url)s" method="get">
                    <div>
                        <input type="hidden" name="render" value="html">
                        <label for="sponsor" class="left">Sponsor:</label>
                        <input type="text" id="sponsor" name="sponsor">
                        <input type="radio" name="sponsor_search" value="name" checked id="sponsor_name">
                        <label for="sponsor_name">name</label>
                        <input type="radio" name="sponsor_search" value="email" id="sponsor_email">
                        <label for="sponsor_email">email</label>
                        <label for="sponsoree" class="left">Sponsoree:</label>
                        <input type="text" id="sponsoree" name="sponsoree">
                        <input type="radio" name="sponsoree_search" value="name" checked id="sponsoree_name">
                        <label for="sponsoree_name">name</label>
                        <input type="radio" name="sponsoree_search" value="email" id="sponsoree_email">
                        <label for="sponsoree_email">email</label>
                        <div>* is a wildcard.</div>
                        <input type="submit" value="Search" class="button">
                    </div>
                </form>
            </div>
            <div id="footer">
                Powered by <a href="%(udd_url)s">UDD</a>.
                <a href="%(source_url)s">Source</a>.
            </div>
        </body>
    </html>""")


html_template = textwrap.dedent("""\
    <!DOCTYPE html>
    <html>
        <head>
            <title>Ubuntu Sponsorship Miner</title>
            <style type="text/css">
                #footer {
                    clear: left;
                    padding-top: 2em;
                }
                table {
                    border-collapse: collapse;
                    margin-bottom: 1em;
                }
            </style>
            <script type="text/javascript" src="%(static_url)s/sorttable.js"></script>
        </head>
        <body>
            <h1>Ubuntu Sponsorship Miner</h1>
            <div id="content">
                <h2>Sponsorships</h2>
                <p>(Table is sortable)</p>
                <table border="1" class="sortable">
                    <thead><tr>
                        <th>Date</th>
                        <th>Sponsor</th>
                        <th>Sponsoree</th>
                        <th>Package</th>
                        <th>Version</th>
                        <th>Distribution</th>
                        <th>Bugs fixed</th>
                        <th>Action</th>
                    </tr></thead>
                    <tbody>
                        %(packages)s
                    </tbody>
                </table>
                <div>View <a href="%(text_url)s">as text</a></div>
                <div>Search <a href="%(url)s">again</a></div>
            </div>
            <div id="footer">
                Powered by <a href="%(udd_url)s">UDD</a>.
                <a href="%(source_url)s">Source</a>.
            </div>
        </body>
    </html>
""")


text_template = textwrap.dedent("""\
    == SPONSOR_NAME ==

    === General feedback ===

    ## Please fill us in on your shared experience. (How many packages did you sponsor? How would you judge the quality? How would you describe the improvements? Do you trust the applicant?)

    === Specific experiences of working together ===

    || Package || Version || Bugs || Action || Notes ||
    %(packages)s

    === Areas of improvement ===

    ## There's always something...
""")


def get_packages_html(sponsorships):
    """Generate package list"""
    packages = []
    for upload in sponsorships:
        packages.append(
                 """<tr>
                        <td>%(date)s</td>
                        <td title="%(signed_by)s">%(signed_by_name)s</td>
                        <td title="%(changed_by)s">%(changed_by_name)s</td>
                        <td><a href="https://launchpad.net/ubuntu/+source/%(source)s">%(source)s</a></td>
                        <td><a href="https://launchpad.net/ubuntu/+source/%(source)s/%(version)s">%(version)s</a></td>
                        <td><a href="https://launchpad.net/ubuntu/%(release)s">%(distribution)s</a></td>
                        <td>
                            %(bugs)s
                        </td>
                        <td>%(info)s</td>
                    </tr>""" % {
                'source': upload.source,
                'version': upload.version,
                'date': upload.date.strftime('%Y-%m-%d %H:%M'),
                'changed_by': html.escape(upload.changed_by, quote=True),
                'changed_by_name': html.escape(upload.changed_by_name, quote=True),
                'signed_by': html.escape(upload.signed_by, quote=True),
                'signed_by_name': html.escape(upload.signed_by_name, quote=True),
                'distribution': upload.distribution,
                'release': upload.release,
                'bugs': ('\n'+' '*28).join(['<a href="https://launchpad.net/bugs/%s">%s</a>' % (bug, bug) for bug in upload.fixed]),
                'info': ' '.join(upload.info),
            })
    return ('\n'+' '*20).join(packages)


def get_packages_text(sponsorships):
    """Generate package list"""
    packages = []
    for upload in sponsorships:
        packages.append(("|| [[https://launchpad.net/ubuntu/+source/%(source)s|%(source)s]] "
            "|| [[https://launchpad.net/ubuntu/+source/%(source)s/%(version)s|%(version)s]] "
            "|| %(bugs)s || %(info)s || ||") % {
                'source': upload.source,
                'version': upload.version,
                'bugs': ' '.join(['[[https://launchpad.net/bugs/%s|%s]]' % (bug, bug) for bug in upload.fixed]),
                'info': ' '.join(upload.info),
            })
    return '\n'.join(packages)


def locate_sponsorships(sponsor, sponsor_search, sponsoree, sponsoree_search):
    """Query UDD for sponsorhips. Yields rows"""

    # String-substituted into query:
    assert sponsor_search in ('name', 'email')
    assert sponsoree_search in ('name', 'email')

    # Make sure we are dealing with Unicode:
    psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
    psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)
    conn = psycopg2.connect(DATABASE)
    conn.set_client_encoding('UTF-8')
    cur = conn.cursor()

    if sponsor:
        sponsor = sponsor.replace('*', '%')
    else:
        sponsor = '%'

    if sponsoree:
        sponsoree = sponsoree.replace('*', '%')
    else:
        sponsoree = '%'

    cur.execute("""
        SELECT source, version, date, changed_by, changed_by_name,
               changed_by_email, signed_by, signed_by_name, signed_by_email,
               distribution,
               array_to_string(ARRAY(
                 SELECT bug
                 FROM ubuntu_upload_history_launchpad_closes
                 WHERE source = ubuntu_upload_history.source
                   AND version = ubuntu_upload_history.version
               ), ' ') AS fixed
        FROM ubuntu_upload_history
        WHERE lower(changed_by_%s) LIKE lower(%%s)
          AND lower(signed_by_%s) LIKE lower(%%s)
          AND signed_by_name <> changed_by_name
        ORDER BY date
        LIMIT %d
    """ % (sponsoree_search, sponsor_search, LIMIT), (sponsoree, sponsor))

    keys = ('source', 'version', 'date', 'changed_by', 'changed_by_name', 'changed_by_email',
            'signed_by', 'signed_by_name', 'signed_by_email', 'distribution', 'fixed')
    for row in cur.fetchall():
        upload = AttrDict(**dict(list(zip(keys, row))))
        yield upload


def mine_sponsorships(sponsorships):
    """Iterate over sponsorships and guess what we can
    yields sponsorhips
    """
    resyncable_re = re.compile(r'[-\d](fakesync|build)\d+$')
    ubuver_re = re.compile(r'[-\d](\d+)?ubuntu(\d+)$')
    for upload in sponsorships:
        upload['fixed'] = upload['fixed'].split()
        upload['release'] = upload.distribution.split('-')[0]
        info = set()

        if upload.distribution.endswith('-proposed'):
            info.add('sru')
        elif upload.distribution.endswith('-security'):
            info.add('security')

        m = resyncable_re.search(upload.version)
        if m:
            if m.group(1) == 'fakesync':
                info.add('sync')
            elif m.group(1) == 'build':
                info.add('rebuild')
            else:
                info.add(m.group(1))

        m = ubuver_re.search(upload.version)
        if m:
            if m.group(1) == '0' and m.group(2) == '1':
                info.add('upgrade')

        upload['info'] = sorted(info)
        # Things we can't determine without changelog / lpapi:
        # sync, merge
        yield upload


def display_sponsorships(sponsor, sponsor_search, sponsoree,
                         sponsoree_search, render):
    """Return rendering of sponsorships"""

    sponsorships = mine_sponsorships(locate_sponsorships(
        sponsor, sponsor_search, sponsoree, sponsoree_search))

    if render == 'html':
        params = {
            'url': URL,
            'text_url': URL + '?' + urlencode({
                'render': 'text',
                'sponsor': sponsor,
                'sponsor_search': sponsor_search,
                'sponsoree': sponsoree,
                'sponsoree_search': sponsoree_search,
            }),
            'static_url': STATIC_URL,
            'udd_url': UDD_URL,
            'source_url': SOURCE_URL,
            'packages': get_packages_html(sponsorships),
        }
        return html_template % params
    else:
        params = {
            'packages': get_packages_text(sponsorships),
        }
        return text_template % params


def display_form():
    """Return rendered search form"""
    params = {
        'url': URL,
        'udd_url': UDD_URL,
        'source_url': SOURCE_URL,
    }
    return form_template % params


def main():
    form = cgi.FieldStorage()

    render = form.getfirst('render', '')
    sponsor = form.getfirst('sponsor', '')
    sponsoree = form.getfirst('sponsoree', '')
    if render and (sponsor or sponsoree):
        sponsor_search = form.getfirst('sponsor_search')
        if sponsor_search not in ('name', 'email'):
            sponsor_search = 'name'
        sponsoree_search = form.getfirst('sponsoree_search')
        if sponsoree_search not in ('name', 'email'):
            sponsoree_search = 'name'
        if render not in ('html', 'text'):
            render = 'html'
        body = display_sponsorships(sponsor, sponsor_search, sponsoree,
                                    sponsoree_search, render)
    else:
        render = 'html'
        body = display_form()

    if not body:
        body = 'ERROR: No body'

    if render == 'text':
        print_contenttype_header('text/plain')
    else:
        print_contenttype_header('text/html')
    print(body)


if __name__ == '__main__':
    main()
